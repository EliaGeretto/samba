Release Announcements
=====================

This is the first pre release of Samba 4.17.  This is *not*
intended for production environments and is designed for testing
purposes only.  Please report any defects via the Samba bug reporting
system at https://bugzilla.samba.org/.

Samba 4.17 will be the next version of the Samba suite.


UPGRADING
=========


NEW FEATURES/CHANGES
====================

Bronze bit and S4U support with MIT Kerberos 1.20
-------------------------------------------------

In 2020 Microsoft Security Response Team received another Kerberos-related
report. Eventually, that led to a security update of the CVE-2020-17049,
Kerberos KDC Security Feature Bypass Vulnerability, also known as a ‘Bronze
Bit’. With this vulnerability, a compromised service that is configured to use
Kerberos constrained delegation feature could tamper with a service ticket that
is not valid for delegation to force the KDC to accept it.

With the release of MIT Kerberos 1.20, Samba AD DC is able able to mitigate the
‘Bronze Bit’ attack. MIT Kerberos KDC's KDB (Kerberos Database Driver) API was
changed to allow passing more details between KDC and KDB components. When built
against MIT Kerberos, Samba AD DC supports MIT Kerberos 1.19 and 1.20 versions
but 'Bronze Bit' mitigation is provided only with MIT Kerberos 1.20.

In addition to fixing the ‘Bronze Bit’ issue, Samba AD DC now fully supports
S4U2Self and S4U2Proxy Kerberos extensions.

Resource Based Constrained Delegation (RBCD) support
----------------------------------------------------

Samba AD DC built with MIT Kerberos 1.20 offers RBCD support now. With MIT
Kerberos 1.20 we have complete RBCD support passing Sambas S4U testsuite.
Note that samba-tool lacks support for setting this up yet!

To complete RBCD support and make it useful to Administrators we added the
Asserted Identity [1] SID into the PAC for constrained delegation. This is
available for Samba AD compiled with MIT Kerberos 1.20.

[1] https://docs.microsoft.com/en-us/windows-server/security/kerberos/kerberos-constrained-delegation-overview


REMOVED FEATURES
================


smb.conf changes
================

  Parameter Name                          Description     Default
  --------------                          -----------     -------


KNOWN ISSUES
============

https://wiki.samba.org/index.php/Release_Planning_for_Samba_4.17#Release_blocking_bugs


#######################################
Reporting bugs & Development Discussion
#######################################

Please discuss this release on the samba-technical mailing list or by
joining the #samba-technical IRC channel on irc.freenode.net.

If you do report problems then please try to send high quality
feedback. If you don't provide vital information to help us track down
the problem then you will probably be ignored.  All bug reports should
be filed under the Samba 4.1 and newer product in the project's Bugzilla
database (https://bugzilla.samba.org/).


======================================================================
== Our Code, Our Bugs, Our Responsibility.
== The Samba Team
======================================================================

